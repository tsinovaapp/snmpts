package br.com.tsinova.snmpts;

import java.util.List;

public class Switch extends Host {
    
    private List<Data> listData;        

    public Switch() {
    }    

    public List<Data> getListData() {
        return listData;
    }

    public void setListData(List<Data> listData) {
        this.listData = listData;
    }    
    
    
}