package br.com.tsinova.snmpts;

public class OutputLogstash extends Output{

    public OutputLogstash() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
}
