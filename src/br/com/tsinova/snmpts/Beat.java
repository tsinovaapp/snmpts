package br.com.tsinova.snmpts;

import org.json.JSONArray;

public class Beat {
    
    private String name;
    private String version;
    private JSONArray tags;  
    private String passwordSudo;
    private String elasticsearchHost;

    public Beat() {
    }

    public String getElasticsearchHost() {
        return elasticsearchHost;
    }

    public void setElasticsearchHost(String elasticsearchHost) {
        this.elasticsearchHost = elasticsearchHost;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public JSONArray getTags() {
        return tags;
    }

    public void setTags(JSONArray tags) {
        this.tags = tags;
    }

    public String getPasswordSudo() {
        return passwordSudo;
    }

    public void setPasswordSudo(String passwordSudo) {
        this.passwordSudo = passwordSudo;
    }    
    
}
