package br.com.tsinova.snmpts;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.Iterator;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Util {

    private static final String CACHE_PORTS_DETAILS = "cache/ports_details.json";
    private static final SimpleDateFormat DATE_FORMAT_US = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("HH:mm:ss.000");

    private static void insertPortDetailsHostCache(JSONArray json, String host, int port,
            long value, int type, Date timestamp) throws Exception {

        JSONObject port_host_details = new JSONObject();
        port_host_details.put("host", host);
        port_host_details.put("port", port);
        port_host_details.put("timestamp", timestamp.getTime());

        if (type == Oid.TRAFFIC_INBOUND) {
            port_host_details.put("inbound", value);
        } else {
            port_host_details.put("outbound", value);
        }

        json.put(port_host_details);

        Files.write(Paths.get(CACHE_PORTS_DETAILS), json.toString().getBytes(), StandardOpenOption.WRITE);

    }

    private static void updatePortDetailsHostCache(JSONArray json, String host, int port,
            long value, int type, Date timestamp) throws Exception {

        Integer index = null;

        for (int i = 0; i < json.length(); i++) {
            JSONObject port_host_details_flag = json.getJSONObject(i);
            if (port_host_details_flag.getString("host").equalsIgnoreCase(host)
                    && port_host_details_flag.getInt("port") == port) {
                index = i;
                break;
            }
        }

        if (index != null) {
            JSONObject obj = json.getJSONObject(index);

            if (type == Oid.TRAFFIC_INBOUND) {
                obj.put("inbound", value);
            } else {
                obj.put("outbound", value);
            }

            obj.put("host", host);
            obj.put("port", port);
            obj.put("timestamp", timestamp.getTime());

            json.put(index, obj);
        }

        Files.write(Paths.get(CACHE_PORTS_DETAILS), json.toString().getBytes(), StandardOpenOption.WRITE);

    }

    public static String getDatetimeForElasticsearch(Date date) {
        String dateFormated = DATE_FORMAT_US.format(date) + "T" + TIME_FORMAT.format(date) + "Z";
        return dateFormated;
    }

    public static String getDatetimeNowForElasticsearch() {
        return Instant.now().toString();
    }

    public static int getSecondsByPeriod(Date dStart, Date dEnd) {
        DateTime start = new DateTime(dStart);
        DateTime end = new DateTime(dEnd);
        int seconds = Seconds.secondsBetween(start, end).getSeconds();
        return seconds;
    }

    public synchronized static double getTrafficPortByHostPort(String host,
            int port, long value, int type, double interval) throws Exception {

        if (value < 0) {
            value = value * -1;
        }

        JSONArray json = new JSONArray(new JSONTokener(new FileInputStream(Paths.get(CACHE_PORTS_DETAILS).toFile())));

        JSONObject port_host_details = null;

        for (int i = 0; i < json.length(); i++) {
            JSONObject port_host_details_flag = json.getJSONObject(i);
            if (port_host_details_flag.getString("host").equalsIgnoreCase(host)
                    && port_host_details_flag.getInt("port") == port) {
                port_host_details = port_host_details_flag;
                break;
            }
        }

        double traffic;

        // Já existe um histórico
        if (port_host_details != null) {

            Date timestamp = new Date(port_host_details.getLong("timestamp"));
            int intervalSecondsPeriod = getSecondsByPeriod(timestamp, new Date());

            // Verifica se o histórico não é antigo/desatualizado
            if (intervalSecondsPeriod < interval * 2) {

                // leitura dos valores antigos                      
                long valueOld;

                try {
                    valueOld = port_host_details.getLong(type == Oid.TRAFFIC_INBOUND ? "inbound" : "outbound");

                } catch (org.json.JSONException ex) {
                    // atualiza o cache
                    updatePortDetailsHostCache(json, host, port, value, type, new Date());
                    return 0;
                }

                // calcula a diferença
                double valueTraffic = value - valueOld;

                // converte para positivo caso seja negativo a diferença
                valueTraffic = valueTraffic < 0 ? (valueTraffic * -1) : valueTraffic;

                // calcula a quantidade de bytes trafegada por segundo
                valueTraffic = valueTraffic / interval;

                // converte para kb
                valueTraffic = valueTraffic / 1000.0;

                // arredonda o número
                valueTraffic = round(valueTraffic, 2);

                traffic = valueTraffic;

            } else {
                traffic = 0;

            }

            // atualiza o cache
            updatePortDetailsHostCache(json, host, port, value, type, new Date());

        } else {
            traffic = 0;

            // cadastra no cache
            insertPortDetailsHostCache(json, host, port, value, type, new Date());

        }

        return traffic;

    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);

        return bd.doubleValue();

    }

    public static Object executeScript(String script, Object value) throws Exception {

        Binding binding = new Binding();
        GroovyShell shell = new GroovyShell(binding);

        Object valueScript;

        if (value instanceof String) {

            valueScript = "\"" + value.toString() + "\"";

        } else if (value instanceof Integer || value instanceof Double || value instanceof Float || value instanceof Long) {

            valueScript = value;

        } else {
            throw new Exception("only strings, integers, doubles, floats, and longs are accepted");

        }

        script = script.replace("{{value}}", valueScript.toString());

        Object response = shell.evaluate(script);

        return response;

    }

    public static int getNumberPortsSwitchByDescription(String description) {
        if (description == null || description.isEmpty()) {
            return 0;
        }
        description = description.trim();
        String text[] = description.split(" ");
        String regexs[] = new String[]{"(\\d+)-Port", "(\\d+)G", "(\\d+)GB", "(\\d+)P", "(\\d+)Port"};
        for (String regex : regexs) {
            for (String partText : text) {
                partText = partText.trim();
                if (partText.matches(regex)) {
                    return Integer.parseInt(partText.replaceAll(regex, "$1"));
                }
            }

        }
        return 0;
    }

    public static String getIdDoc(String idDocActionUpdateLast, JSONObject document) throws Exception {

        Iterator it = document.keys();

        while (it.hasNext()) {

            String key = it.next().toString();
            Object value = document.get(key);

            if (value instanceof JSONObject) {

                JSONObject objectChildren = document.getJSONObject(key);
                Iterator itObjectChildren = objectChildren.keys();

                while (itObjectChildren.hasNext()) {
                    String keyChildren = itObjectChildren.next().toString();
                    Object valueByChildren = objectChildren.get(keyChildren);

                    if (idDocActionUpdateLast.contains("{" + key + "." + keyChildren + "}")) {
                        idDocActionUpdateLast = idDocActionUpdateLast.replace("{" + key + "." + keyChildren + "}", (valueByChildren + ""));
                    }

                }

            } else {

                if (idDocActionUpdateLast.contains("{" + key + "}")) {
                    idDocActionUpdateLast = idDocActionUpdateLast.replace("{" + key + "}", (value + ""));
                }

            }

        }

        return idDocActionUpdateLast;

    }
    
//    public static void main(String args[]) throws Exception{
//        
//        String id = "switch_{serial_num}_port_{ports.port}_latest_port";
//        
//        
//        JSONObject json = new JSONObject();
//        //json.put("serial_num", "KLOUSADR");
//        
//        JSONObject port = new JSONObject();
//        port.put("port", 10);
//        
//        json.put("ports", port);
//        
//        
//        
//        String newId = getIdDoc(id, json);
//        
//        System.out.println(newId);
//        
//        
//    }
    
    

}
