package br.com.tsinova.snmpts;

import java.io.FileInputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class Service extends Thread {

    private final List<ReadSend> listReadSends;
    private List<Switch> listSwitches;
    private List<Output> listOutputs;
    private Beat beat;

    public Service() {
        listReadSends = new ArrayList<>();
    }

    public List<Switch> getListSwitches(JSONObject json) throws Exception {

        List<Switch> listSwitches = new ArrayList<>();

        JSONArray arraySwitches = json.getJSONArray("switches");

        for (int i = 0; i < arraySwitches.length(); i++) {

            JSONObject switcheJSON = arraySwitches.getJSONObject(i);

            Switch switche = new Switch();
            switche.setHost(switcheJSON.getString("host"));
            switche.setPort(switcheJSON.getInt("port"));
            switche.setVersion(switcheJSON.getString("version"));
            switche.setCommunity(switcheJSON.getString("community"));

            JSONArray listDataJSON = switcheJSON.getJSONArray("data");
            List<Data> listData = new ArrayList<>();

            for (int j = 0; j < listDataJSON.length(); j++) {

                JSONObject dataJSON = listDataJSON.getJSONObject(j);

                Data data = new Data();
                data.setInterval(dataJSON.getInt("interval"));
                if (dataJSON.has("type")) {
                    data.setType(dataJSON.getString("type"));
                }
                if (dataJSON.has("action")) {
                    data.setAction(dataJSON.getString("action"));
                }
                if (dataJSON.has("id_doc_action_update_last")) {
                    data.setIdDocActionUpdateLast(dataJSON.getString("id_doc_action_update_last"));
                }

                JSONArray listOidsJSON = dataJSON.getJSONArray("oids");

                List<Oid> listOids = new ArrayList<>();

                for (int k = 0; k < listOidsJSON.length(); k++) {

                    JSONObject oidJSON = listOidsJSON.getJSONObject(k);

                    Oid oid = new Oid();
                    oid.setName(oidJSON.getString("name"));
                    oid.setOid(oidJSON.getString("oid"));
                    oid.setType(oidJSON.getInt("type"));
                    if (oidJSON.has("default")) {
                        oid.setValueDefault(oidJSON.get("default"));
                    }
                    if (oidJSON.has("script")) {
                        oid.setScript(oidJSON.getString("script"));
                    }
                    if (oidJSON.has("default_if_exists")) {
                        oid.setUseDefaultIfExists(true);
                        oid.setDefaultIfExists(oidJSON.get("default_if_exists"));
                    }
                    if (oidJSON.has("join_oid")) {
                        oid.setUseJoinOid(true);
                        oid.setJoinOid(oidJSON.getString("join_oid"));
                    }
                    if (oidJSON.has("add_doc_devices")) {
                        oid.setAddDocDevices((oidJSON.getInt("add_doc_devices") == 1));
                    }
                    if (oidJSON.has("enabled")) {
                        oid.setEnabled((oidJSON.getInt("enabled") == 1));
                    }
                    if (oidJSON.has("sum")) {
                        JSONArray listSumJson = oidJSON.getJSONArray("sum");
                        List<Sum> listSum = new ArrayList<>();
                        for (int l = 0; l < listSumJson.length(); l++) {
                            Sum sum = new Sum(listSumJson.getJSONObject(l));
                            listSum.add(sum);
                        }
                        oid.setListSum(listSum);
                    }

                    if (oid.getType() == 3) {

                        JSONArray listOidsOIDJSON = oidJSON.getJSONArray("oids");
                        List<Oid> listOidsOid = new ArrayList<>();

                        for (int l = 0; l < listOidsOIDJSON.length(); l++) {

                            JSONObject oidOIDJson = listOidsOIDJSON.getJSONObject(l);

                            Oid oidOID = new Oid();
                            oidOID.setName(oidOIDJson.getString("name"));
                            oidOID.setOid(oidOIDJson.getString("oid"));
                            oidOID.setType(oidOIDJson.getInt("type"));
                            if (oidOIDJson.has("default")) {
                                oidOID.setValueDefault(oidOIDJson.get("default"));
                            }
                            if (oidOIDJson.has("script")) {
                                oidOID.setScript(oidOIDJson.getString("script"));
                            }
                            if (oidOIDJson.has("default_if_exists")) {
                                oidOID.setUseDefaultIfExists(true);
                                oidOID.setDefaultIfExists(oidOIDJson.get("default_if_exists"));
                            }
                            if (oidOIDJson.has("add_doc_devices")) {
                                oidOID.setAddDocDevices((oidOIDJson.getInt("add_doc_devices") == 1));
                            }
                            if (oidOIDJson.has("enabled")) {
                                oidOID.setEnabled((oidOIDJson.getInt("enabled") == 1));
                            }
                            if (oidOIDJson.has("join_oid")) {
                                oidOID.setUseJoinOid(true);
                                oidOID.setJoinOid(oidOIDJson.getString("join_oid"));
                            }
                            if (oidOIDJson.has("sum")) {
                                JSONArray listSumJson = oidOIDJson.getJSONArray("sum");
                                List<Sum> listSum = new ArrayList<>();
                                for (int h = 0; h < listSumJson.length(); h++) {
                                    Sum sum = new Sum(listSumJson.getJSONObject(h));
                                    listSum.add(sum);
                                }
                                oidOID.setListSum(listSum);
                            }

                            listOidsOid.add(oidOID);

                        }

                        oid.setOids(listOidsOid);

                    }

                    listOids.add(oid);

                }

                data.setListOid(listOids);
                listData.add(data);

            }

            switche.setListData(listData);
            listSwitches.add(switche);

        }

        return listSwitches;

    }

    private Beat getBeat(JSONObject json) throws Exception {
        JSONObject beatJSON = json.getJSONObject("beat");
        Beat beat = new Beat();
        beat.setName(beatJSON.getString("name"));
        beat.setVersion(beatJSON.getString("version"));
        beat.setTags(beatJSON.getJSONArray("tags"));
        beat.setPasswordSudo(beatJSON.getString("password_sudo"));
        if (beatJSON.has("elasticsearch_host")) {
            beat.setElasticsearchHost(beatJSON.getString("elasticsearch_host"));
        }
        return beat;
    }

    public List<Output> getListOutputs(JSONObject json) throws Exception {

        JSONObject outputJSON = json.getJSONObject("output");
        List<Output> listOutputs = new ArrayList<>();

        if (outputJSON.has("logstash")) {
            OutputLogstash outputLogstash = new OutputLogstash();
            outputLogstash.setHost(outputJSON.getJSONObject("logstash").getString("host"));
            outputLogstash.setPort(outputJSON.getJSONObject("logstash").getInt("port"));
            listOutputs.add(outputLogstash);
        }

        return listOutputs;

    }

    @Override
    public void run() {

        // Efetua a leitura dos parâmetros ...
        System.out.println("Reading parameters ...");

        try {
            JSONObject json = new JSONObject(new JSONTokener(new FileInputStream(Paths.get("snmpts.json").toFile())));

            listSwitches = getListSwitches(json);
            listOutputs = getListOutputs(json);
            beat = getBeat(json);

            System.out.println("Successful reading");

        } catch (Exception ex) {
            System.err.println("Failed to read");
            ex.printStackTrace();
            return;

        }

        while (true) {

            Map<String, Object> plugin = null;

            try {
                plugin = Params.getParamsPlugin(beat.getElasticsearchHost(), "rede_params_plugin");
            } catch (Exception ex) {
            }

            Integer interval = (plugin != null
                    && plugin.containsKey("interval")) ? Integer.parseInt(plugin.get("interval").toString())
                    : null;

            if (interval != null) {

                Iterator<ReadSend> it = listReadSends.iterator();

                while (it.hasNext()) {
                    ReadSend readSend = it.next();

                    if (readSend.data.getType().equals("About")) {
                        continue;
                    }

                    if (readSend.data.getInterval() != interval) {

                        try {
                            readSend.close();
                            readSend.interrupt();
                            readSend.join();
                            System.out.println("Closed read " + readSend.switche.getHost() + ", Type " + readSend.data.getType());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        it.remove();

                    }

                }

            }

            for (Switch switche : listSwitches) {

                for (Data data : switche.getListData()) {

                    if (data.getType().equals("About")
                            && !listReadSends.stream()
                                    .anyMatch(rs
                                            -> rs.data.getType().equals("About"))) {

                        ReadSend readSend = new ReadSend(switche, data, listOutputs, beat);
                        readSend.start();
                        listReadSends.add(readSend);

                    } else {

                        boolean hasExists = listReadSends.stream()
                                .anyMatch(rs
                                        -> (rs.switche.getHost().equals(switche.getHost())
                                && rs.data.getInterval() == data.getInterval()
                                && rs.data.getType().equals(data.getType()))
                                );

                        if (!hasExists) {

                            if (interval != null) {
                                data.setInterval(interval); // define o novo intervalo                            
                            }

                            ReadSend readSend = new ReadSend(switche, data, listOutputs, beat);
                            readSend.start();
                            listReadSends.add(readSend);

                            System.out.println("Read and send processes successfully started "
                                    + "for host " + switche.getHost());

                        }

                    }

                }

            }

            try {
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
            }

        }

        // inicia processos
        /*System.out.println("Starting reading and sending processes ...");

        for (Switch switche : listSwitches) {
            for (Data data : switche.getListData()) {
                ReadSend readSend = new ReadSend(switche, data, listOutputs, beat);
                readSend.start();
                listReadSends.add(readSend);
            }
        }

        System.out.println("Read and send processes successfully started");*/
    }

    public static void main(String[] args) {

        Service service = new Service();
        service.start();

    }

}
