package br.com.tsinova.snmpts;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TreeEvent;
import org.snmp4j.util.TreeUtils;

public class ReadSend extends Thread {

    public final Switch switche;
    public final Data data;
    private final List<Output> listOutput;
    private boolean run;
    private final Beat beat;

    public ReadSend(Switch switche, Data data, List<Output> listOutput, Beat beat) {
        this.switche = switche;
        this.data = data;
        this.listOutput = listOutput;
        this.run = true;
        this.beat = beat;
    }

    public void close() {
        this.run = false;
    }

    private List<Oid> getLisOidByType(List<Oid> list, int... types) {
        List<Oid> newList = new ArrayList<>();
        for (Oid oid : list) {
            if (!oid.isEnabled()) {
                continue;
            }
            for (int i = 0; i < types.length; i++) {
                if (oid.getType() == types[i]) {
                    newList.add(oid);
                    break;
                }
            }
        }
        return newList;
    }

    private Oid getOidByType(List<Oid> list, int type) {
        for (Oid oid : list) {
            if (!oid.isEnabled()) {
                continue;
            }
            if (oid.getType() == type) {
                return oid;
            }
        }
        return null;
    }

    private CommunityTarget getTarget(Host host) {
        CommunityTarget target = new CommunityTarget();
        if (host.getVersion().equals("2c")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + host.getHost() + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version2c);
        } else if (host.getVersion().equals("1")) {
            target.setCommunity(new OctetString(host.getCommunity()));
            target.setAddress(GenericAddress.parse("udp:" + host.getHost() + "/" + host.getPort()));
            target.setRetries(2);
            target.setTimeout(1500);
            target.setVersion(SnmpConstants.version1);
        }
        return target;
    }

    private PDU getPDU(List<Oid> listOids, String add) {
        PDU pdu = new PDU();
        pdu.setType(PDU.GET);
        listOids.forEach((oid) -> {
            pdu.add(new VariableBinding(new OID("." + oid.getOid() + add)));
        });
        return pdu;
    }

//    private PDU getPDU(List<Oid> listOids) {
//        PDU pdu = new PDU();
//        pdu.setType(PDU.GET);
//        listOids.forEach((oid) -> {
//            pdu.add(new VariableBinding(new OID("." + oid.getOid())));
//        });
//        return pdu;
//    }
//
//    private PDU getPDU(String oid) {
//        PDU pdu = new PDU();
//        pdu.setType(PDU.GET);
//        pdu.add(new VariableBinding(new OID("." + oid)));
//        return pdu;
//    }
    private PDU getPDU(Oid oid) {
        PDU pdu = new PDU();
        pdu.setType(PDU.GET);
        pdu.add(new VariableBinding(new OID("." + oid.getOid())));
        return pdu;
    }

    /**
     * Converte o valor armazenado em um OID em um tipo específico
     *
     * @param type
     * @param variableBinding
     * @return
     * @throws Exception
     */
    private Object getValueOID(int type, VariableBinding variableBinding) throws Exception {
        switch (type) {
            case Oid.INTEGER:
                // inteiro
                return variableBinding.getVariable().toInt();
            case Oid.TEXT:
                // texto
                return variableBinding.getVariable().toString();
            case Oid.LONG:
                // Long
                return variableBinding.getVariable().toLong();
            case Oid.DOUBLE:
                // Double
                return Double.parseDouble(variableBinding.getVariable().toString());
            case Oid.NUM_PORTS:
                // oid para número de portas retorna a descrição do switch, um texto
                return variableBinding.getVariable().toString();
            default:
                throw new Exception("Invalid variable type");
        }
    }

//    /**
//     * Retorna o valor armazenado em um OID de um host
//     *
//     * @param target
//     * @param snmp
//     * @param listOidsPort
//     * @param add
//     * @return
//     * @throws Exception
//     */
//    private JSONObject getResponseOidsSimplePort(CommunityTarget target, Snmp snmp, List<Oid> listOidsPort, String add, Integer port) throws Exception {
//
//        List<Oid> listOidsSimple = getLisOidByType(listOidsPort, Oid.INTEGER,
//                Oid.TEXT, Oid.LONG, Oid.COUNTER_REGISTER, Oid.TRAFFIC_INBOUND, Oid.TRAFFIC_OUTBOUND);
//
//        ResponseEvent responseEvent = snmp.send(getPDU(listOidsSimple, add), target);
//        PDU response = responseEvent.getResponse();
//
//        JSONObject json = new JSONObject();
//
//        for (Oid oid : listOidsSimple) {
//            boolean flag = false;
//
//            for (int i = 0; i < response.size(); i++) {
//
//                VariableBinding variableBinding = response.get(i);
//
//                if (variableBinding.getOid().toString().equalsIgnoreCase(oid.getOid() + add)) {
//
//                    if (variableBinding.isException() || variableBinding.getVariable().isException()
//                            || !variableBinding.getOid().isValid() || variableBinding.getOid().isException()) {
//                        json.put(oid.getName(), oid.getValueDefault());
//
//                    } else if (oid.isUseDefaultIfExists()) {
//                        json.put(oid.getName(), oid.getDefaultIfExists());
//
//                    } else {
//
//                        if (oid.isUseJoinOid()) {
//                            String join_id = oid.getJoinOid() + "." + variableBinding.getVariable().toString();
//                            Oid newOid = new Oid();
//                            newOid.setName(oid.getName());
//                            newOid.setOid(join_id);
//                            newOid.setType(2);
//                            newOid.setValueDefault("");
//                            JSONObject jsonJsonId = getResponseOidsSimple(target, Arrays.asList(newOid), "", 0);
//                            json.put(oid.getName(), jsonJsonId.get(oid.getName()));
//
//                        } else {
//
//                            Object value;
//
//                            if (oid.getType() == Oid.TRAFFIC_INBOUND) {
//    double traffic = Util.getTrafficPortByHostPort(switche.getHost(),
//            port, (int) getValueOID(Oid.INTEGER, variableBinding),
//            oid.getType(), data.getInterval());
//                                value = traffic;
//
//                            } else if (oid.getType() == Oid.TRAFFIC_OUTBOUND) {
//                                double traffic = Util.getTrafficPortByHostPort(switche.getHost(),
//                                        port, (int) getValueOID(Oid.INTEGER, variableBinding),
//                                        oid.getType(), data.getInterval());
//                                value = traffic;
//
//                            } else {
//                                value = getValueOID(oid.getType(), variableBinding);
//
//                            }
//
//                            if (oid.getScript() != null && !oid.getScript().isEmpty()) {
//                                value = Util.executeScript(oid.getScript(), value);
//
//                            }
//
//                            json.put(oid.getName(), value);
//
//                        }
//
//                    }
//
//                    flag = true;
//                    break;
//
//                }
//
//            }
//
//            if (!flag) {
//                json.put(oid.getName(), oid.getValueDefault());
//            }
//
//        }
//
//        return json;
//
//    }
//    /**
//     * Retorna um valor armazenado em um OID de um host
//     *
//     * @param target
//     * @param listOids
//     * @param add
//     * @return
//     * @throws Exception
//     */
//    private JSONObject getResponseOidsSimple(CommunityTarget target, List<Oid> listOids, String add, Integer port) throws Exception {
//        TransportMapping transport = new DefaultUdpTransportMapping();
//        Snmp snmp = new Snmp(transport);
//        transport.listen();
//
//        // leitura de variavel simples da porta
//        JSONObject jsonResponseOidsSimple = getResponseOidsSimplePort(target, snmp, listOids, add, port);
//
//        snmp.close();
//        transport.close();
//
//        return jsonResponseOidsSimple;
//
//    }
//    /**
//     * Retorna a lista de portas de um determinado host
//     *
//     * @param target
//     * @param snmp
//     * @param oid
//     * @return
//     */
//    private ArrayList<Integer> getListPorts(CommunityTarget target, Snmp snmp, Oid oid) {
//        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
//        List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));
//        ArrayList<Integer> ports = new ArrayList<>();
//        for (TreeEvent event : events) {
//            if (event == null) {
//                continue;
//            }
//            if (event.isError()) {
//                continue;
//            }
//            VariableBinding[] varBindings = event.getVariableBindings();
//            for (VariableBinding varBinding : varBindings) {
//                if (varBinding == null) {
//                    continue;
//                }
//                ports.add(varBinding.getVariable().toInt());
//            }
//        }
//        return ports;
//
//    }
    /**
     * Retorna a quantidade de registros de um OID de um host
     *
     * @param target
     * @param snmp
     * @param oid
     * @return
     */
    private int getCounterByOid(CommunityTarget target, Snmp snmp, Oid oid) {
        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));
        int counter = 0;
        for (TreeEvent event : events) {
            if (event == null) {
                continue;
            }
            if (event.isError()) {
                continue;
            }
            VariableBinding[] varBindings = event.getVariableBindings();
            for (VariableBinding varBinding : varBindings) {
                if (varBinding == null) {
                    continue;
                }
                counter++;
            }
        }
        return counter;

    }

//    private JSONArray getResponseOidPorts(CommunityTarget target, Oid oid) throws Exception {
//        TransportMapping transport = new DefaultUdpTransportMapping();
//        Snmp snmp = new Snmp(transport);
//        transport.listen();
//
//        JSONArray json = new JSONArray();
//        ArrayList<Integer> ports = getListPorts(target, snmp, oid);
//
//        for (Integer port : ports) {
//            JSONObject jsonPort = getResponseOidsSimple(target, oid.getOids(), "." + port, port);
//            jsonPort.put("port", port);
//            json.put(jsonPort);
//
//        }
//
//        snmp.close();
//        transport.close();
//
//        return json;
//
//    }
    private JSONObject getResponseOidsCounter(CommunityTarget target, List<Oid> oids) throws Exception {
        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        JSONObject json = new JSONObject();

        for (Oid oid : oids) {
            int counter = getCounterByOid(target, snmp, oid);
            json.put(oid.getName(), counter);
        }

        snmp.close();
        transport.close();

        return json;
    }

    private JSONObject getJson(JSONObject jsonValuesDefault, JSONObject jsonValues) throws JSONException {

        JSONObject json = new JSONObject();

        Iterator it = jsonValuesDefault.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValuesDefault.get(key));
        }

        it = jsonValues.keys();
        while (it.hasNext()) {
            String key = it.next().toString();
            json.put(key, jsonValues.get(key));
        }

        return json;
    }

    /**
     * Retorna o valor de uma oid
     *
     * @param target
     * @param oid
     * @param add
     * @param response
     * @return
     * @throws Exception
     */
    public Object getValueOidByPDUResponse(CommunityTarget target, Oid oid, String add, PDU response) throws Exception {

        Object value = null;

        for (int i = 0; i < response.size(); i++) {

            VariableBinding variableBinding = response.get(i);

            if (!variableBinding.getOid().toString().equalsIgnoreCase(oid.getOid() + add)) {
                continue;
            }

            if (variableBinding.isException() || variableBinding.getVariable().isException()
                    || !variableBinding.getOid().isValid() || variableBinding.getOid().isException()) {
                value = null;

            } else if (oid.isUseDefaultIfExists()) {
                value = oid.getDefaultIfExists();

            } else {

                if (oid.isUseJoinOid()) {

                    Oid newOid = new Oid(oid.getJoinOid() + "." + variableBinding.getVariable().toString(),
                            oid.getName(), Oid.TEXT, "", true);
                    value = getResponseOid(target, newOid);

                } else {
                    value = getValueOID(oid.getType(), variableBinding);

                }

            }

            break;

        }

        return value;

    }

    /**
     * Retorna o valor de um oid específico
     *
     * @param target
     * @param oid
     * @return
     * @throws Exception
     */
    public Object getResponseOid(CommunityTarget target, Oid oid) throws Exception {

        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        ResponseEvent responseEvent = snmp.send(getPDU(oid), target);
        PDU response = responseEvent.getResponse();

        Object value = getValueOidByPDUResponse(target, oid, "", response);

        snmp.close();
        transport.close();

        return value;

    }

    /**
     * Leitura de variaveis do tipo INTEIRO, TEXTO, DOUBLE, LONG E NÚMERO DE
     * PORTSA
     *
     * @param target
     * @param listOids
     * @param add
     * @return
     * @throws Exception
     */
    public JSONObject getResponseOidsBasic(CommunityTarget target, List<Oid> listOids, String add) throws Exception {

        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        ResponseEvent responseEvent = snmp.send(getPDU(listOids, add), target);
        PDU response = responseEvent.getResponse();

        JSONObject json = new JSONObject();

        for (Oid oid : listOids) {

            Object value = getValueOidByPDUResponse(target, oid, add, response);

            if (value == null) {
                value = oid.getValueDefault();

            } else if (oid.getType() == Oid.NUM_PORTS) {
                value = Util.getNumberPortsSwitchByDescription(value.toString());

            } else if (oid.getScript() != null && !oid.getScript().isEmpty()) {
                value = Util.executeScript(oid.getScript(), value);

            }

            json.put(oid.getName(), value);

        }

        snmp.close();
        transport.close();

        return json;

    }

    /**
     * Leitura de variaveis somatórios
     *
     * @param target
     * @param listOids
     * @return
     * @throws Exception
     */
    public JSONObject getResponseOidsSummation(CommunityTarget target, List<Oid> listOids) throws Exception {

        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        JSONObject json = new JSONObject();

        for (Oid oid : listOids) {

            long sum = 0;

            TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
            List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));

            for (TreeEvent event : events) {
                if (event == null) {
                    continue;
                }
                if (event.isError()) {
                    continue;
                }
                VariableBinding[] varBindings = event.getVariableBindings();
                for (VariableBinding varBinding : varBindings) {
                    if (varBinding == null) {
                        continue;
                    }
                    sum += (long) getValueOID(Oid.LONG, varBinding);
                }
            }

            Object value = sum;

            if (oid.getScript() != null && !oid.getScript().isEmpty()) {
                value = Util.executeScript(oid.getScript(), value);

            }

            json.put(oid.getName(), value);

        }

        snmp.close();
        transport.close();

        return json;

    }

    /**
     * Retorna uma lista de objetos/valores com base em uma oid
     *
     * @param target
     * @param oid
     * @param typeChildren
     * @return
     * @throws Exception
     */
    public List<Object> getResponseObjectsByOid(CommunityTarget target, Oid oid, int typeChildren) throws Exception {

        ArrayList<Object> list = new ArrayList<>();

        TransportMapping transport = new DefaultUdpTransportMapping();
        Snmp snmp = new Snmp(transport);
        transport.listen();

        TreeUtils treeUtils = new TreeUtils(snmp, new DefaultPDUFactory());
        List<TreeEvent> events = treeUtils.getSubtree(target, new OID("." + oid.getOid()));

        for (TreeEvent event : events) {
            if (event == null) {
                continue;
            }
            if (event.isError()) {
                continue;
            }
            VariableBinding[] varBindings = event.getVariableBindings();
            for (VariableBinding varBinding : varBindings) {
                if (varBinding == null) {
                    continue;
                }
                list.add(getValueOID(typeChildren, varBinding));
            }
        }

        snmp.close();
        transport.close();

        return list;

    }

    @Override
    public void run() {

        while (run) {

            // efetuando a leitura de oids simples
            System.out.println("Reading the Switch OIDs " + switche.getHost() + ", Interval " + data.getInterval() + "s, Type " + (data.getType() == null ? "" : data.getType()));

            CommunityTarget target = getTarget(switche);
            JSONObject jsonOidsBasic = new JSONObject();
            JSONObject jsonOidsSummation = new JSONObject();
            JSONObject jsonOidsCounters = new JSONObject();
            JSONArray jsonArrayPorts = new JSONArray();

            // Leitura de oids básicas
            List<Oid> listOidsBasic = getLisOidByType(data.getListOid(),
                    Oid.INTEGER,
                    Oid.LONG,
                    Oid.TEXT,
                    Oid.DOUBLE,
                    Oid.NUM_PORTS);
            if (!listOidsBasic.isEmpty()) {
                try {
                    jsonOidsBasic = getResponseOidsBasic(target, listOidsBasic, "");
                } catch (Exception ex) {
                    for (Oid oid : listOidsBasic) {
                        try {
                            jsonOidsBasic.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            // Leitura de oids somatórios
            List<Oid> listOidsSummation = getLisOidByType(data.getListOid(),
                    Oid.TRAFFIC_INBOUND,
                    Oid.TRAFFIC_OUTBOUND);
            if (!listOidsSummation.isEmpty()) {
                try {
                    jsonOidsSummation = getResponseOidsSummation(target, listOidsSummation);
                } catch (Exception ex) {
                    for (Oid oid : listOidsSummation) {
                        try {
                            jsonOidsSummation.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            // Leitura de oids contadores
            List<Oid> listOidsCounters = getLisOidByType(data.getListOid(),
                    Oid.COUNTER_REGISTER);
            if (!listOidsCounters.isEmpty()) {
                try {
                    jsonOidsCounters = getResponseOidsCounter(target, listOidsCounters);
                } catch (Exception ex) {
                    for (Oid oid : listOidsCounters) {
                        try {
                            jsonOidsCounters.put(oid.getName(), oid.getValueDefault());
                        } catch (JSONException ex1) {
                        }
                    }
                }
            }

            // Leitura da oid porta
            Oid oidPorts = getOidByType(data.getListOid(), Oid.PORTS);
            if (oidPorts != null) {
                try {
                    List<Object> list = getResponseObjectsByOid(target, oidPorts, Oid.INTEGER);
                    int numPortsReal = Util.getNumberPortsSwitchByDescription(getResponseOid(target,
                            new Oid("1.3.6.1.2.1.1.1.0", "num_ports", Oid.TEXT, "", true)).toString());
                    Iterator<Object> it = list.iterator();
                    while (it.hasNext()) {
                        int port = (int) it.next();
                        if (port > numPortsReal || port < 1) {
                            it.remove();
                        }
                    }
                    for (Object value : list) {
                        int port = (int) value;
                        try {
                            JSONObject jsonOidsPort = getResponseOidsBasic(target, oidPorts.getOids(), "." + port);
                            jsonOidsPort.put("port", port);
                            jsonArrayPorts.put(jsonOidsPort);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

//            JSONObject jsonOidsSimple = new JSONObject();
//            JSONArray jsonOidsPort = new JSONArray();
//            Oid oidPorts;
//
//            CommunityTarget target = getTarget(switche);
//
//            // Leitura de oids simples / OID = Value            
//            List<Oid> listOidsSimple = getLisOidByType(data.getListOid(), Oid.INTEGER,
//                    Oid.LONG, Oid.TEXT, Oid.DOUBLE, Oid.NUM_PORTS);
//            if (!listOidsSimple.isEmpty()) {
//                try {
//                    jsonOidsSimple = getResponseOidsSimple(target, listOidsSimple, "", null);
//                } catch (Exception ex) {
//                    for (Oid oid : listOidsSimple) {
//                        try {
//                            jsonOidsSimple.put(oid.getName(), oid.getValueDefault());
//                        } catch (JSONException ex1) {
//                        }
//                    }
//                }
//            }
//
//            // Efetuando a leitura Oid portas
//            oidPorts = getOidByType(data.getListOid(), Oid.PORTS);
//
//            if (oidPorts != null) {
//
//                try {
//
//                    jsonOidsPort = getResponseOidPorts(target, oidPorts);
//
//                    if (oidPorts.getListSum() != null) {
//
//                        for (Sum sum : oidPorts.getListSum()) {
//
//                            if (!sum.isEnabled()) {
//                                continue;
//                            }
//
//                            double total = 0;
//
//                            for (int i = 0; i < jsonOidsPort.length(); i++) {
//
//                                JSONObject portJson = jsonOidsPort.getJSONObject(i);
//                                Object value = portJson.get(sum.getOidName());
//
//                                if (sum.getScript() != null) {
//                                    value = Util.executeScript(sum.getScript(), value);
//                                }
//
//                                if (value instanceof Integer) {
//                                    total += (int) value;
//                                } else {
//                                    total += (double) value;
//                                }
//
//                            }
//
//                            // verifica se o número é inteiro
//                            if (total == (int) total) { // é inteiro
//                                jsonOidsSimple.put(sum.getName(), (int) total);
//                            } else {
//                                jsonOidsSimple.put(sum.getName(), total);
//                            }
//
//                        }
//                    }
//
//                } catch (Exception ex) {
//                    ex.printStackTrace();
//                    try {
//                        if (oidPorts.getListSum() != null) {
//                            for (Sum sum : oidPorts.getListSum()) {
//                                jsonOidsSimple.put(sum.getName(), sum.getValueDefault());
//                            }
//                        }
//                    } catch (JSONException ex1) {
//                    }
//
//                }
//
//            }
//
//            // Efetuando a leitura de Oids contadores
//            List<Oid> listOidsCounter = getLisOidByType(data.getListOid(), Oid.COUNTER_REGISTER);
//            if (!listOidsCounter.isEmpty()) {
//                try {
//                    JSONObject responseOidsCounter = getResponseOidsCounter(target, listOidsCounter);
//                    for (Oid oid : listOidsCounter) {
//                        jsonOidsSimple.put(oid.getName(), responseOidsCounter.get(oid.getName()));
//                    }
//                } catch (Exception ex) {
//                    for (Oid oid : listOidsCounter) {
//                        try {
//                            jsonOidsSimple.put(oid.getName(), oid.getValueDefault());
//                        } catch (JSONException ex1) {
//                        }
//                    }
//                }
//            }
            // Seta outros atributos, atributos que qualquer documento tem
            JSONObject jsonDefault = new JSONObject();

            try {

                jsonDefault.put("host", switche.getHost());
                jsonDefault.put("timestamp", Util.getDatetimeNowForElasticsearch());

                JSONObject metadataJSON = new JSONObject();
                metadataJSON.put("beat", beat.getName());
                metadataJSON.put("version", beat.getVersion());
                jsonDefault.put("@metadata", metadataJSON);

                JSONObject beatJSON = new JSONObject();
                beatJSON.put("name", beat.getName());
                beatJSON.put("version", beat.getVersion());
                jsonDefault.put("beat", beatJSON);

                jsonDefault.put("tags", beat.getTags());
                jsonDefault.put("interval", data.getInterval());

            } catch (JSONException ex) {
            }

//            try {
//
//                jsonDefault = getJson(jsonDefault, jsonOidsBasic);
//                jsonDefault = getJson(jsonDefault, jsonOidsCounters);
//                jsonDefault = getJson(jsonDefault, jsonOidsSummation);
//
//                if (jsonArrayPorts.length() == 0) {
//
//                    System.out.println(jsonDefault.toString());
//                    System.out.println("");
//                    System.out.println("============================================================");
//
//                } else {
//
//                    for (int i = 0; i < jsonArrayPorts.length(); i++) {
//                        JSONObject jsonPort = new JSONObject();
//                        jsonPort.put(oidPorts.getName(), jsonArrayPorts.getJSONObject(i));
//                        JSONObject json = getJson(jsonDefault, jsonPort);
//                        System.out.println(json.toString());
//                        System.out.println("*************************************************************");
//                    }
//
//                    System.out.println("============================================================");
//
//                }
//
//            } catch (Exception ex) {
//            }
            for (Output out : listOutput) {

                try {

                    jsonDefault = getJson(jsonDefault, jsonOidsBasic);
                    jsonDefault = getJson(jsonDefault, jsonOidsCounters);
                    jsonDefault = getJson(jsonDefault, jsonOidsSummation);

                    if (jsonArrayPorts.length() == 0) {

                        // add novo doc
                        if ((data.getAction() == null
                                || data.getAction().equalsIgnoreCase("append")
                                || data.getAction().equalsIgnoreCase("append_and_update_last"))) {
                            if (data.getType() != null && !data.getType().isEmpty()) {
                                try {
                                    jsonDefault.put("type", data.getType());
                                } catch (JSONException ex) {
                                }
                            }
                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonDefault.toString());
                                os.flush();
                            }
                        }

                        // update doc
                        if (data.getAction() != null && (data.getAction().equalsIgnoreCase("update_last")
                                || data.getAction().equalsIgnoreCase("append_and_update_last"))
                                && data.getIdDocActionUpdateLast() != null) {
                            if (data.getType() != null && !data.getType().isEmpty()) {
                                try {
                                    jsonDefault.put("type", "Latest" + data.getType());
                                } catch (JSONException ex) {
                                }
                            }
                            jsonDefault.put("id_doc", Util.getIdDoc(data.getIdDocActionUpdateLast(), jsonDefault));
                            try (Socket socket = new Socket(out.host, out.port);
                                    DataOutputStream os = new DataOutputStream(
                                            new BufferedOutputStream(socket.getOutputStream()))) {
                                os.writeBytes(jsonDefault.toString());
                                os.flush();
                            }
                        }

                        System.out.println(jsonDefault.toString());
                        System.out.println("");
                        System.out.println("============================================================");

                    } else {

                        for (int i = 0; i < jsonArrayPorts.length(); i++) {

                            JSONObject jsonPort = new JSONObject();
                            jsonPort.put(oidPorts.getName(), jsonArrayPorts.getJSONObject(i));

                            // add novo doc
                            if ((data.getAction() == null
                                    || data.getAction().equalsIgnoreCase("append")
                                    || data.getAction().equalsIgnoreCase("append_and_update_last"))) {
                                JSONObject json = getJson(jsonDefault, jsonPort);
                                if (data.getType() != null && !data.getType().isEmpty()) {
                                    try {
                                        json.put("type", data.getType());
                                    } catch (JSONException ex) {
                                    }
                                }
                                try (Socket socket = new Socket(out.host, out.port);
                                        DataOutputStream os = new DataOutputStream(
                                                new BufferedOutputStream(socket.getOutputStream()))) {
                                    os.writeBytes(json.toString());
                                    os.flush();
                                }
                                System.out.println(json.toString());
                            }

                            // update doc
                            if (data.getAction() != null && (data.getAction().equalsIgnoreCase("update_last")
                                    || data.getAction().equalsIgnoreCase("append_and_update_last"))
                                    && data.getIdDocActionUpdateLast() != null) {
                                JSONObject json = getJson(jsonDefault, jsonPort);
                                if (data.getType() != null && !data.getType().isEmpty()) {
                                    try {
                                        json.put("type", "Latest" + data.getType());
                                    } catch (JSONException ex) {
                                    }
                                }
                                json.put("id_doc", Util.getIdDoc(data.getIdDocActionUpdateLast(), json));
                                try (Socket socket = new Socket(out.host, out.port);
                                        DataOutputStream os = new DataOutputStream(
                                                new BufferedOutputStream(socket.getOutputStream()))) {
                                    os.writeBytes(json.toString());
                                    os.flush();
                                }
                                System.out.println(json.toString());
                            }
                            
                            System.out.println("*************************************************************");
                        }

                        System.out.println("============================================================");

                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

//            System.out.println("Data switch: " + jsonOidsSimple.toString());
//            System.out.println("Data ports: " + jsonOidsPort.toString());
//
//            try {
//
//                for (Output out : listOutput) {
//
//                    // Gravar variáveis simples = Atributo = Valor 
//                    if (jsonOidsSimple.length() > 0) {
//                        try (Socket socket = new Socket(out.host, out.port);
//                                DataOutputStream os = new DataOutputStream(
//                                        new BufferedOutputStream(socket.getOutputStream()))) {
//                            os.writeBytes(getJson(json, jsonOidsSimple).toString());
//                            os.flush();
//                        }
//                    }
//
//                    // Gravar variaveis das portas
//                    if (jsonOidsPort.length() > 0) {
//
//                        for (int i = 0; i < jsonOidsPort.length(); i++) {
//
//                            JSONObject jsonPort = jsonOidsPort.getJSONObject(i);
//
//                            // Encaminhar portas
//                            try (Socket socket = new Socket(out.host, out.port);
//                                    DataOutputStream os = new DataOutputStream(
//                                            new BufferedOutputStream(socket.getOutputStream()))) {
//                                JSONObject jsonValues = new JSONObject();
//                                jsonValues.put(oidPorts.getName(), jsonPort);
//                                os.writeBytes(getJson(json, jsonValues).toString());
//                                os.flush();
//                            }
//
//                        }
//
//                    }
//
//                    System.out.println("Data sent to " + out.host + ":" + out.port);
//
//                }
//
//            } catch (Exception ex) {
//            }
            // Intervalo de espera
            try {
                Thread.sleep(data.getInterval() * 1000);
            } catch (InterruptedException ex) {
            }

        }

    }

}
