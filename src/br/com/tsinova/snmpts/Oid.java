package br.com.tsinova.snmpts;

import java.util.List;

public class Oid {
    
    public static final int INTEGER = 1, 
            TEXT = 2, 
            PORTS = 3, 
            LONG = 4, 
            COUNTER_REGISTER = 5, 
            TRAFFIC_INBOUND = 7,
            TRAFFIC_OUTBOUND = 8,
            DOUBLE = 9,
            NUM_PORTS = 10;
    
    private String oid;
    private String name;
    private String script;
    private int type; // 1 = inteiro, 2 = texto, 3 = portas, 4 = Long, 5 = Contador, 7 = Tráfego de entrada, 8 = Tráfego de saída, 9 = Double
    private List<Oid> oids;
    private Object valueDefault;
    private Object defaultIfExists;
    private boolean useDefaultIfExists;
    private String joinOid;
    private boolean useJoinOid;
    private List<Sum> listSum;
    private boolean addDocDevices;
    private boolean enabled;

    public Oid() {
        this.useDefaultIfExists = false;
        this.useJoinOid = false;
        this.addDocDevices = false;
        this.enabled = true;
    }

    public Oid(String oid, String name, int type, Object valueDefault, boolean enabled) {
        this.oid = oid;
        this.name = name;
        this.type = type;
        this.valueDefault = valueDefault;
        this.enabled = enabled;
        this.useDefaultIfExists = false;
        this.useJoinOid = false;
        this.addDocDevices = false;
    }    
    
    public String getOid() {
        return oid;
    }

    public void setOid(String oid) {
        this.oid = oid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }       

    public List<Oid> getOids() {
        return oids;
    }

    public void setOids(List<Oid> oids) {
        this.oids = oids;
    } 

    public Object getValueDefault() {
        return valueDefault;
    }

    public void setValueDefault(Object valueDefault) {
        this.valueDefault = valueDefault;
    }

    public Object getDefaultIfExists() {
        return defaultIfExists;
    }

    public void setDefaultIfExists(Object defaultIfExists) {
        this.defaultIfExists = defaultIfExists;
    }

    public boolean isUseDefaultIfExists() {
        return useDefaultIfExists;
    }

    public void setUseDefaultIfExists(boolean useDefaultIfExists) {
        this.useDefaultIfExists = useDefaultIfExists;
    }

    public String getJoinOid() {
        return joinOid;
    }

    public void setJoinOid(String joinOid) {
        this.joinOid = joinOid;
    }    

    public boolean isUseJoinOid() {
        return useJoinOid;
    }

    public void setUseJoinOid(boolean useJoinOid) {
        this.useJoinOid = useJoinOid;
    }

    public String getScript() {
        return script;
    }

    public void setScript(String script) {
        this.script = script;
    }    

    public List<Sum> getListSum() {
        return listSum;
    }

    public void setListSum(List<Sum> listSum) {
        this.listSum = listSum;
    }    

    public boolean isAddDocDevices() {
        return addDocDevices;
    }

    public void setAddDocDevices(boolean addDocDevices) {
        this.addDocDevices = addDocDevices;
    }    

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "Oid{" + "oid=" + oid + ", name=" + name + ", type=" + type + ", oids=" + oids + '}';
    }       
    
}
